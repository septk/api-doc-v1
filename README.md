# Server OpenAPI Specification
[![Build Status](https://travis-ci.org/septk/api-doc-v1.svg?branch=master)](https://travis-ci.org/septk/api-doc-v1)

## Links

- Documentation(ReDoc): https://septk.github.io/api-doc-v1/
- SwaggerUI: https://septk.github.io/api-doc-v1/swagger-ui/
- Look full spec:
    + JSON https://septk.github.io/api-doc-v1/swagger.json
    + YAML https://septk.github.io/api-doc-v1/swagger.yaml
- Preview spec version for branch `[branch]`: https://septk.github.io/api-doc-v1/preview/[branch]

**Warning:** All above links are updated only after Travis CI finishes deployment
